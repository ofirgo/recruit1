@extends('layouts.app')

@section('title', 'Interviews')

@section('content')

<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Summary</th><th>Date</th><th>Candidate name</th><th>Owner name</th>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->summary}}</td>
            <td>{{$interview->date}}</td>
            <td>
              @if(isset($interview->candidate_id))
                {{$interview->candidate->name}}  
              @else
                  None
              @endif           
            </td>
            <td>

              @if(isset($interview->user_id))
                {{$interview->user->name}}  
              @else
                None
              @endif  
        
            </td>
    @endforeach
</table>
@endsection

