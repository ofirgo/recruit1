@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "summary">Summary</label>
            <input type = "summary" class="form-control" name = "summary">
        </div>     
        <div class="form-group">
            <label for = "date">Date</label>
            <input type = "date" class="form-control" name = "date">
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Save">
        </div>  

                    <div class="form-group row">
                            <label for="candidate_id" class="col-md-4 col-form-label text-md-right">Selecte candidate</label>
                            <div class="col-md-6">
                                <select class="form-control" name="candidate_id">                                                                         
                                                                                                      
                                   @foreach ($candidates as $candidate)
                                     <option value="{{ $candidate->id }}"> 
                                         {{ $candidate->name }} 
                                     </option>
                                   @endforeach    

                                 </select>
                            </div>
                        </div>

        <div class="form-group row">
        <label for="department_id" class="col-md-4 col-form-label text-md-right">Selecte user</label>
        <div class="col-md-6">
   
            <select class="form-control" name="user_id">  
        @if (null != App\User::owner($candidate->candiate_id))    
            @if (isset($candidate->candidate_id))                                              
                    <option value=""> 
                        <a  > {{$candidate->user->name}}</a> 
                    </option>
                 
            @else  
                @foreach($users as $user)
                <option value="{{$user->id}}"> 
                    <a  > {{$user->name}}</a> 
                </option>   
                @endforeach
            @endif
        @endif
        </select>
        </div>

        </form>    
@endsection