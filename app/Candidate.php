<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Candidate extends Model
{
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }     
    public function interviews(){
        return $this->hasMany('App\Interview');
    }
    // public static function can($candidate_id){
    //     $names = DB::table('candidates')->where('id',$candidate_id)->pluck('id');
    //     return self::find($names)->all(); 
    // }
}
