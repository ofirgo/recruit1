<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all();
        $candidates = Candidate::all();  
        return view('interviews.index', compact('interviews','candidates'));
    }
    
    public function chooseCandidate($id, $cid = null){
        $interview = Interview::findOrFail($id);
        $interview->candidate_id = $cid;
        $interview->save(); 
        return back();

    }

    public function chooseCandidateFromInteview(Request $request){
        $id = $request->id;
        $cid = $request->candidate_id;
        $interview = Interview::findOrFail($id);
        $interview->candidate_id = $cid;
        $interview->save(); 
        return back();

    }


    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews= $user->interviews;
        $users = User::all();
        if(isset($interviews)) return view('interviews.index', compact('interviews','users'));
       
        return ('You dont have any interview');
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   Gate::authorize('careate-inteview');
        $interviews = Interview::all();
        $candidates = Candidate::all(); 
        $users = User::all(); 
        return view('interviews.create', compact('interviews','candidates','users')); 

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $interview = new Interview(); 
        $inter = $interview->create($request->all());

        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $interviews = Interview::findOrFail($id);
        // return view('interviews.create', compact('interviews')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
